<?php
/**
 * class ElasticQuery|Firesphere\ElasticSearch\Queries\ElasticQuery Base of an Elastic Query
 *
 * @package Firesphere\Elastic\Search
 * @author Simon `Firesphere` Erkelens; Marco `Sheepy` Hermo
 * @copyright Copyright (c) 2018 - now() Firesphere & Sheepy
 */

namespace Firesphere\ElasticSearch\Queries;

use Firesphere\SearchBackend\Interfaces\QueryInterface;
use Firesphere\SearchBackend\Queries\CoreQuery;
use SilverStripe\Core\Injector\Injectable;

/**
 * Class CoreQuery is the base of every query executed.
 *
 * Build a query to execute against Elastic. Uses as simplee as possible an interface.
 *
 * Merely a shim around CoreQuery for convenience
 *
 * @package Firesphere\Elastic\Search
 */
class ElasticQuery extends CoreQuery implements QueryInterface
{
    use Injectable;
}
